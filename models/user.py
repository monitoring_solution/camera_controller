from pydantic import BaseModel


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str or None = None


class User(BaseModel):
    username: str
    emal: str or None = None
    hashed_passwd: str
    disabled: bool or None = None
    access: str or None
    access_token: str or None


class UserInDb(User):
    hashed_passwd: str
