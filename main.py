import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from routs.users import user
from validator.camera_capture import video

description = """
Walidator Zajętości miejsc parkingowych API  🚀

## Functionality

* **Create user**
* **Read user**
* **Login as a user**
"""

tags_metadata = [
    {
        "name": "Users",
        "description": "Operations with users. The **login** logic is also here.",
    },
    {
        "name": "Video",
        "description": "Vide capture",
    }
]

app = FastAPI(
    title="Inf_pabwl_lab_4",
    description=description,
    version="0.0.1",
    contact={
        "name": "Maciej Urbaniak",
        "email": "macieju1223@gmail.com",
    },
    openapi_tags=tags_metadata
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*']
)

app.include_router(user, tags=["Users"])
app.include_router(video, tags=["Video"])

if __name__ == "__main__":
    uvicorn.run(
        "main:app",
        reload=True,
        host="0.0.0.0",
        port=8000,
    )
