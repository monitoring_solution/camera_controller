FROM python:3.11

RUN apt-get update -y && \
    apt-get install -y libgl1-mesa-glx libglib2.0-0

COPY . /.

RUN pip install -r requirements.txt

CMD ["python3", "main.py"]
