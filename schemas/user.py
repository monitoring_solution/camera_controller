def userEntity(item) -> dict:
    return {
        "_id": str(item["_id"]),
        item["username"]: {
            "username": item["username"],
            "email": str(item["emal"]),
            "hashed_password": str(item["hashed_password"]),
            "disabled": str(item["disabled"]),
            "access": str(item["access"])
        }
    }


def usersEntity(entiti) -> list:
    return [userEntity(item) for item in entiti]
