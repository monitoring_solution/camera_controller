import json
from fastapi import FastAPI, Path
from fastapi.responses import StreamingResponse
from validator.check_space import ParkingSpaceMonitoring
from fastapi import APIRouter

path = "http://192.168.43.115:5000/video_feed"
videos = {
    "file1.mp4",
    "file2.mp4",
    "video_feed"
}

video = APIRouter()


def video_name_validate(video_name):
    if not video_name.endswith(".mp4"):
        video_name += ".mp4"
    if video_name not in videos:
        return {"error": "Capture not Found"}
    video_file_path = path + video_name
    return video_file_path


@video.get("/video/{video_name}")
def stream_video(video_name: str = Path(..., title="Vide Name")):
    def iterate_file():
        with open(path, mode="rb") as V_capture:
            yield from V_capture
    return StreamingResponse(iterate_file(), media_type="video/mp4")


@video.get('/getbays/{video_name}')
def get_space_counter(video_name: str = Path(..., title="Vide Name")):
    video_name_validate(video_name)
    json_file = "validator/parking/spaceCounter/" + video_name + ".json"
    with open(json_file, 'r') as file:
        data = json.load(file)
    return data

@video.get('/video_capture/{video_format}')
async def capture_video_img(video_format: str = Path(..., title="format")):
    """
    Capture video images and stream them in the specified format.
    Avalible formats:
    - imgGray
    - imgBlur
    - imgThreshold
    - imgMedian
    - imgDialate
    """
    # file = "file2"
    # path = "validator/parking/file2.mp4"
    file = "stream_pi"
    path = "http://192.168.43.115:8000/stream.mjpg"
    video = ParkingSpaceMonitoring(path, file)
    return StreamingResponse(content=video.show_video_formats(video_format), media_type="multipart/x-mixed-replace;boundary=frame")


@video.get('/video_bays/')
async def capture_video_bays():
    # file = "file2"
    # path = "validator/parking/file2.mp4"
    file = "stream_pi"
    path = "http://192.168.43.115:8000/stream.mjpg"
    video = ParkingSpaceMonitoring(path, file)
    return StreamingResponse(content=video.validate_video_parking_space(), media_type="multipart/x-mixed-replace;boundary=frame")