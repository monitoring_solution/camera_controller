import json

import cv2
import pickle
import cvzone
import numpy as np

from pprint import pprint


class ParkingSpaceMonitoring:
    def __init__(self, path, file):
        self.width = 100
        self.height = 100

        self.file_path = path
        self.file_name = file
        self.cap = cv2.VideoCapture(path)

        self.posList = self.load_positions()

    def load_positions(self):
        try:
            with open(f'validator/parking/parkingPositions/{self.file_name}', 'rb') as file:
                return pickle.load(file)
        except:
            return []

    def save_positions(self):
        with open(f'validator/parking/parkingPositions/{self.file_name}', 'wb') as file:
            pickle.dump(self.posList, file)

    def mouse_click(self, events, x, y, flags, params):
        """
            Generates Parking monitoring bay and delets it left/right mouse click (useful stuff)
            """
        place = len(self.posList)
        if events == cv2.EVENT_LBUTTONDOWN:
            self.posList.append({f'A{place}': (x, y)})
        if events == cv2.EVENT_RBUTTONDOWN:
            self.posList.pop()
        self.save_positions()

    def space_counter(self, spaceCounter):
        save_to_file = f"validator/parking/spaceCounter/{self.file_name}.json"
        file_content = {
            "Space": {
                "Bays": str(len(self.posList)),
                "Free": str(spaceCounter),
                "Busy": str(len(self.posList) - spaceCounter)
            }
        }
        # print(file_content)
        json_data = json.dumps(file_content, indent=4)
        with open(save_to_file, 'w') as file:
            file.write(json_data)

    def check_parking_space(self, img_process, kernel, img):
        spaceCounter = 0
        for i, pos in enumerate(self.posList):
            x, y = pos[f'A{i}']

            imgCrop = img_process[y:y + self.height, x:x + self.width]
            imgDialate = cv2.dilate(imgCrop, kernel, iterations=1)
            # cv2.imshow(str(x*y), imgDialate)

            pixels_count = cv2.countNonZero(imgDialate)
            cvzone.putTextRect(img, str(pixels_count), (x, y + self.height - 1), scale=0.5, thickness=1, offset=0)

            if pixels_count < 130:
                color = (0, 255, 0)
                spaceCounter += 1
            else:
                color = (0, 0, 255)
            cv2.rectangle(img, pos[f'A{i}'], (pos[f'A{i}'][0] + self.width, pos[f'A{i}'][1] + self.height), color, 1)

        cvzone.putTextRect(img, f"{spaceCounter}/{len(self.posList)}", (5, 15), scale=0.8, thickness=1, offset=8,
                           colorR=(0, 200, 0))
        self.space_counter(spaceCounter)

    @staticmethod
    def format_video_capture(img):
        img_format = {}
        # Convert image to gray
        img_format["imgGray"] = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img_format["imgBlur"] = cv2.GaussianBlur(img_format["imgGray"], (3, 3), 1)
        # convert image into binary
        img_format["imgThreshold"] = cv2.adaptiveThreshold(
            img_format["imgBlur"],
            255,
            cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
            cv2.THRESH_BINARY_INV,
            25, 16
        )
        img_format["imgMedian"] = cv2.medianBlur(img_format["imgThreshold"], 5)
        img_format["kernel"] = np.ones((3, 3), np.uint8)
        img_format["imgDialate"] = cv2.dilate(img_format["imgMedian"], img_format["kernel"], iterations=1)

        return img_format

    @staticmethod
    def show_video_capture(img, img_format):
        # Normal image
        cv2.imshow("Image", img)
        # Bluerd img
        # cv2.imshow("ImageBlur", img_format["imgBlur"])
        # Img Threshold
        # cv2.imshow("ImageThreshold", img_format["imgThreshold"])
        # img median
        # cv2.imshow("ImageMedian", img_format["imgMedian"])
        pass

    def run(self):
        while True:
            if self.cap.get(cv2.CAP_PROP_POS_FRAMES) == self.cap.get(cv2.CAP_PROP_FRAME_COUNT):
                self.cap.set(cv2.CAP_PROP_POS_FRAMES, 0)

            success, img = self.cap.read()
            img_format = self.format_video_capture(img)

            self.check_parking_space(img_format["imgDialate"], img_format["kernel"], img)
            self.show_video_capture(img, img_format)

            cv2.setMouseCallback("Image", self.mouse_click)

            cv2.waitKey(10)

    def show_video_formats(self, video_format):
        """ methood for fastapi service """
        videos = ["imgGray", "imgBlur", "imgThreshold", "imgMedian", "imgDialate"]
        if video_format not in videos:
            video_format = "imgDialate"
        while True:
            if self.cap.get(cv2.CAP_PROP_POS_FRAMES) == self.cap.get(cv2.CAP_PROP_FRAME_COUNT):
                self.cap.set(cv2.CAP_PROP_POS_FRAMES, 0)

            success, img = self.cap.read()
            if not success:
                break
            else:
                img_format = self.format_video_capture(img)
                _, buffer = cv2.imencode('.jpg', img_format[video_format])
                frame_bytes = buffer.tobytes()
                yield (b'--frame\r\n'
                       b'Content-Type: image/jpeg\r\n\r\n' + frame_bytes + b'\r\n')
            cv2.waitKey(10)

    def validate_video_parking_space(self):
        while True:
            if self.cap.get(cv2.CAP_PROP_POS_FRAMES) == self.cap.get(cv2.CAP_PROP_FRAME_COUNT):
                self.cap.set(cv2.CAP_PROP_POS_FRAMES, 0)

            success, img = self.cap.read()
            if not success:
                break
            else:
                img_format = self.format_video_capture(img)
                self.check_parking_space(img_format["imgDialate"], img_format["kernel"], img)
                _, buffer = cv2.imencode('.jpg', img)
                frame_bytes = buffer.tobytes()
                yield (b'--frame\r\n'
                       b'Content-Type: image/jpeg\r\n\r\n' + frame_bytes + b'\r\n')
            cv2.waitKey(10)


if __name__ == "__main__":
    # path = "validator/parking/file2.mp4"
    # file = "file2"
    file = "stream_pi"
    path = "http://192.168.43.115:8000/stream.mjpg"
    parking_monitor = ParkingSpaceMonitoring(path, file)
    parking_monitor.run()
