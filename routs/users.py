from fastapi import APIRouter
from bson.objectid import ObjectId

from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm, OAuth2PasswordBearer
from pydantic import BaseModel
from datetime import datetime, timedelta
from jose import JWTError, jwt
from passlib.context import CryptContext

from config.db_engine import db_users
from schemas.user import usersEntity, userEntity
from models.user import Token, TokenData, User, UserInDb


user = APIRouter()


SECRET_KEY = "316ce855d062b00eaab670ccf981c92beb814a80982e58f6fba1d3725b1c34d1"
ALGORITHM = "HS256"

passwd_context = CryptContext(schemes=['bcrypt'], deprecated="auto")
oauth_2_scheme = OAuth2PasswordBearer(tokenUrl="token")


def verify_passwd(plain_passwd, hashed_passwd):
    return passwd_context.verify(plain_passwd, hashed_passwd)


def get_passwd_hash(password):
    return passwd_context.hash(password)


def get_user(db, username: str):
    for member in db.find():
        if username in member:
            return dict(member[username])


def authenticate_user(db, username: str, password: str):
    user = get_user(db, username)
    if not user:
        return False
    if not verify_passwd(password, user['hashed_passwd']):
        return False
    return user


def create_access_token(data: str, expires_delta: timedelta or None = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)

    to_encode.update({'exp': expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


async def get_current_user(token: str = Depends(oauth_2_scheme)):
    credential_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Coud not validate creds",
        headers={"WWW-Authenticate": "Bearer"}
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credential_exception
        token_data = TokenData(username=username)

    except JWTError:
        raise credential_exception

    user = get_user(db_users, username=token_data.username)
    if user is None:
        raise credential_exception

    return user


async def get_current_active_user(current_user: UserInDb = Depends(get_current_user)):
    if current_user['disabled']:
        raise HTTPException(status_code=400, detail="Inactive User")

    return current_user


@user.post("/token", response_model=Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user = authenticate_user(db_users, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorect Username or Password",
            headers={"WWW-Authenticate": "Bearer"}
        )
    access_token = create_access_token(
        data={"sub": user['username']},
    )
    return {"access_token": access_token, "token_type": "bearer"}


@user.get("/users/me/", response_model=User)
async def read_user_me(current_user: User = Depends(get_current_user)):
    return current_user


@user.get("/users/me/items")
async def read_own_items(current_user: User = Depends(get_current_active_user)):
    return [{"item_id": 1, "owner": current_user}]


@user.post("/users/me/post")
async def add_user(
        name: str,
        passwd: str,
        email: str,
        full_name: str,
        access_lvl: str,
        current_user: User = Depends(get_current_user)
    ):
    """
        Only Admins can add new members
        access_lvl:
            - Admin
            - Developer
            - Member
    """
    if not current_user['access'] == "Admin":
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Insufficient privileges",
            headers={"WWW-Authenticate": "Bearer"}
        )
    new_user = {
        name: {
            "username": name,
            "full_name": full_name,
            "emal": email,
            "hashed_passwd": get_passwd_hash(passwd),
            "disabled": False,
            "access": access_lvl,
            "access_token": "<sccesstoken>"
        }
    }
    db_users.insert_one(dict(new_user))
    return new_user


@user.get('/users')
async def get_all_users(current_user: User = Depends(get_current_user)):
    if not current_user['access'] == "Admin":
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Insufficient privileges",
            headers={"WWW-Authenticate": "Bearer"}
        )

    members = {}
    for member in db_users.find():
        keys = list(member.keys())
        name = keys[1]
        id = keys[0]

        members.update({
            "_id": str(member[id]),
            name: {
                "username": name,
                "emal": str(member[name]['emal']),
                "hashed_passwd": str(member[name]['hashed_passwd']),
                "disabled": str(member[name]['disabled']),
                "access": str(member[name]['access']),
                "access_token": str(member[name]['access_token'])
            }
        })
    return members


# @user.put('/users/update/{member}')
# async def get_user(member):
#     """
#         Function Updates already existing user by its ID
#     """
#     for user in db_users.find():
#         print(user)
#         if user['username'] == member:
#             print(user)
#             uu = db_users.find_one(
#                 {"_id": member['_id']},
#                 {"$set": ObjectId}
#             )
#             print(uu)
