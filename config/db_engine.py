import pymongo


client = pymongo.MongoClient("mongodb://mongodb:27017/")
# client = pymongo.MongoClient("mongodb://localhost:27017/")

db = client["backend"]
""" DB Collections """
db_users = db.collection["Users"]
db_contractors = db.collection["Contractors"]
db_suppliers = db.collection['Suppliers']
db_goods = db.colleciton["Goods"]
db_orders = db.collection["Orders"]

collections = client.backend.list_collection_names()
